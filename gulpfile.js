var gulp = require("gulp"),
	 util = require("gulp-util"),
	 sass = require("gulp-ruby-sass"),
	 autoprefixer = require('gulp-autoprefixer'),
	 minifycss = require('gulp-minify-css'),
	 rename = require('gulp-rename'),
	 log = util.log,
	 cleanCSS = require('gulp-clean-css'),
	 zip = require('gulp-zip');

var mainScss = 'dist/scss/screen.scss',
	 printScss = 'dist/scss/print.scss',
	 ieScss = 'dist/scss/ie.scss';

function compileSCSS(inputSCSSFile)
{
	return sass(inputSCSSFile)
		.on('error', sass.logError)
		.pipe(autoprefixer('last 3 version','safari 5', 'ie 8', 'ie 9'))
		.pipe(gulp.dest('src/styles/'))
		.pipe(rename({suffix: '.min'}))
		.pipe(minifycss())
		.pipe(gulp.dest('src/styles/min/'))
}

gulp.task('sass', function () {
	return compileSCSS(mainScss);
});

gulp.task('sass-ie', function () {
	return compileSCSS(ieScss);
});

gulp.task('sass-print', function () {
	return compileSCSS(printScss);
});

gulp.task('watch', function(){
	log('Watching scss files for modifications');
	gulp.watch(mainScss, ['sass']);
});

gulp.task('default', function (){
	console.log('Gulps working')
});

gulp.task('build', function () {
	return gulp.src('src/*')
		.pipe(zip('build.zip'))
		.pipe(gulp.dest('builds'));
});