/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};

/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {

/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;

/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};

/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);

/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;

/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}


/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;

/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;

/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";

/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ function(module, exports) {

	//var $ = require("jquery");

	function fillGlass($empty, $wine)
	{
		var glassEl = '#glass';
		var glass = Snap(glassEl);

		glass.attr(
			'fill',
			'L(0, 0, 0, 100)#fff:'+$empty+'-#d065a5:'+$wine+''
		);
	}

	$(function(){

		var glass = Snap('#glass');

		glass.attr(
			'fill',
			'#d065a5'
		);

	});

	$(window).scroll(function(){

		var  offset = window.pageYOffset;
		var  wheight = $(window).height();

		if(offset > 200) {
			var customOffset = offset - 200;
			var filledPercent = ( customOffset / wheight ) * 100;

			console.log('Custom offset: ' + customOffset);
			console.log('Viewport Height:' + wheight);
			console.log( filledPercent );

			fillGlass(filledPercent, filledPercent);
		}

	});

/***/ }
/******/ ]);