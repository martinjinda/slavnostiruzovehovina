
function fillGlass($empty, $wine)
{
	var glass = Snap('#glass');
	glass.attr(
		'fill',
		'L(0, 0, 0, 100)#fff:'+$empty+'-#d065a5:'+$wine+''
	);
}

/**
 * Fill the glass with pink color
 */
$(function(){
	var glass = Snap('#glass');
	glass.attr( 'fill', '#d065a5');
});


/**
 * When scrolling uncover schedule
 */
//$(function(){
//	$(window).scroll(function(){
//
//		var offset = window.pageYOffset;
//		var hT = $('#glass').offset().top;
//		var hH = 775;
//		hT = hT - hH;
//		var wH = $(window).height();
//		var wS = $(this).scrollTop();
//
//		if(offset >= hT) {
//			var customOffset = offset - hT;
//			var filledPercent = ( customOffset / 775 ) * 100;
//			fillGlass(filledPercent, filledPercent);
//		}
//
//	});
//});
