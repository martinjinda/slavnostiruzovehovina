module.exports = {
	entry: "./src/scripts/main.js",
	output: {
		path: __dirname,
		filename: "./src/scripts/bundle.js"
	},
	module: {
		loaders: [
			{
				test: require.resolve('snapsvg'),
				loader: 'imports-loader?this=>window,fix=>module.exports=0'
			}
		]
	}
};